from typing import Any, Tuple
import numpy as np
import pandas as pd

from constant import INSTABLE, STABLE

class MSIPredictor:
    def __init__(self, data_file: str):
        self.data_file = data_file
        self.data = pd.read_csv(data_file, sep="\t")

    def instability_calculation(self, vector: np.array) -> Tuple[Any, bool]:
        """
        Функция для расчета вероятности нестабильности микросателлита.

        Args:
            vector (np.array): Вектор длиной 20, представляющий длину повтора.

        Returns:
            probability -> float: Вероятность нестабильности микросателлита.
            is_stable -> boolean : Стабильный или нестабильный повтор
        """
        
        # Нормализуем входной вектор
        vector = np.mean(vector)

        # Выбираем только числовые колонки
        numeric_data = self.data.select_dtypes(include=[np.number])

        # Заменяем значения Nan, поскольку z-score ломается
        numeric_data.fillna(numeric_data.mean(), inplace=True)

        # Вычисляем среднее значение длины повтора
        mean_length = np.nanmean(numeric_data.values)

        # Вычисляем стандартное отклонение длины повтора
        std_length = np.nanstd(numeric_data.values)

        z_score = (vector - mean_length) / std_length
        # Вычисляем вероятность нестабильности с помощью кумулятивной функции распределения стандартного нормального распределения
        probability = 1 - np.exp(-0.5 * (z_score ** 2))
        # Определяем стабильность в сравнении с p-value 
        is_stable = np.all(probability < 0.05)
        if is_stable:
            return probability, STABLE
        return probability, INSTABLE
