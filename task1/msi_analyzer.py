import pandas as pd
from constant import INPUT_FILE, OUTPUT_FILE
from msi_predictor import MSIPredictor

class MSIAnalyzer:
    def __init__(self, input_file: str, output_file: str):
        self.input_file = input_file
        self.output_file = output_file
        self.output_dict = self.extract_vector()
        self.msiPredictor= MSIPredictor(self.input_file)

    def extract_vector(self) -> dict:
        """
        Output model:
        {
            'MSI_KEY1': {
                'STR_KEY1': [values],
                'STR_KEY2': [values]
            },
            'MSI_KEY2': {
                'STR_KEY3': [values],
                'STR_KEY4': [values]
            }
        }
        """
        stability_table = pd.read_csv(self.input_file, sep="\t")
        output_dict = {}
        for col in stability_table.columns:
            if col.startswith("MSI"): 
                msi_key = col
                output_dict[msi_key] = {}
                str_key = None
                str_values = []
                for value in stability_table[col]:
                    if isinstance(value, str):
                        if value.startswith("STR"):
                            if str_key:
                                output_dict[msi_key][str_key] = str_values
                                str_values = []
                            str_key = value
                        else:
                            str_values.append(value)
                    else:
                        str_values.append("")
                if str_key:
                    output_dict[msi_key][str_key] = str_values
        return output_dict
    
    def convert_probability_to_float(self) -> dict:
        """
        {
            'MSI_KEY1': {
                'STR_KEY1': [float_values],
                'STR_KEY2': [float_values]
            },
            'MSI_KEY2': {
                'STR_KEY3': [float_values],
                'STR_KEY4': [float_values]
            }
        }
        """
        for msi_key in self.output_dict.keys():
            for str_key, str_values in self.output_dict[msi_key].items():
                float_values = [float(val) if val else 0.0 for val in str_values]
                self.output_dict[msi_key][str_key] = float_values
        return self.output_dict

    def calculate_probability(self) -> dict:
        """
        {
            'MSI_KEY1': {
                'STR_KEY1': {
                    'values': [float_values],
                    'prob': {
                        'probability': float,
                        'stable': bool
                    }
                },
        }
        """
        for msi_key in self.output_dict.keys():
            for str_key, values in self.output_dict[msi_key].items():
                for val in values:
                    probability, stable = self.msiPredictor.instability_calculation(val)
                self.output_dict[msi_key][str_key] = {"values": values, "prob": {"probability": probability, "stable": stable }}
        return self.output_dict

    def create_dataframe(self) -> pd.DataFrame:
        """
        pd.DataFrame:
            - MSI Key
            - STR Key
            - Probability
            - Stable
        """
        data = []
        for msi_key, msi_values in self.output_dict.items():
            for str_key, str_values in msi_values.items():
                prob = str_values['prob']
                data.append({
                    'MSI Key': msi_key,
                    'STR Key': str_key,
                    'Probability': prob['probability'],
                    'Stable':  prob['stable']
                })
        df = pd.DataFrame(data)
        df.to_csv(self.output_file, sep='\t')
        return df

if __name__ == '__main__':
    analyzer = MSIAnalyzer(INPUT_FILE,OUTPUT_FILE)
    analyzer.output_dict = analyzer.convert_probability_to_float()
    analyzer.output_dict = analyzer.calculate_probability()
    output_frame = analyzer.create_dataframe()
    print(output_frame)