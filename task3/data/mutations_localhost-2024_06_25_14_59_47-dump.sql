--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5 (Debian 14.5-1.pgdg110+1)
-- Dumped by pg_dump version 15.1 (Ubuntu 15.1-1.pgdg22.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: morgan
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO morgan;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: morgan
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: analysis; Type: TABLE; Schema: public; Owner: morgan
--

CREATE TABLE public.analysis (
    analysis_name integer,
    barcode_name character varying(45),
    analysis_role character varying
);


ALTER TABLE public.analysis OWNER TO morgan;

--
-- Name: barcode; Type: TABLE; Schema: public; Owner: morgan
--

CREATE TABLE public.barcode (
    case_id integer,
    patient_id integer,
    barcode_id character varying(45),
    barcode_name character varying(45)
);


ALTER TABLE public.barcode OWNER TO morgan;

--
-- Name: case; Type: TABLE; Schema: public; Owner: morgan
--

CREATE TABLE public."case" (
    case_id integer,
    patient_id integer
);


ALTER TABLE public."case" OWNER TO morgan;

--
-- Name: mutation; Type: TABLE; Schema: public; Owner: morgan
--

CREATE TABLE public.mutation (
    mutation_id integer
);


ALTER TABLE public.mutation OWNER TO morgan;

--
-- Name: mutations_result; Type: TABLE; Schema: public; Owner: morgan
--

CREATE TABLE public.mutations_result (
    id_mutation_result integer,
    zygosity character varying,
    mutation_id integer,
    analysis_analysis_name integer
);


ALTER TABLE public.mutations_result OWNER TO morgan;

--
-- Name: patient; Type: TABLE; Schema: public; Owner: morgan
--

CREATE TABLE public.patient (
    patient_id integer,
    patient_name character varying(45)
);


ALTER TABLE public.patient OWNER TO morgan;

--
-- Data for Name: analysis; Type: TABLE DATA; Schema: public; Owner: morgan
--

COPY public.analysis (analysis_name, barcode_name, analysis_role) FROM stdin;
1	test	Major
\.


--
-- Data for Name: barcode; Type: TABLE DATA; Schema: public; Owner: morgan
--

COPY public.barcode (case_id, patient_id, barcode_id, barcode_name) FROM stdin;
1	1	1	test
\.


--
-- Data for Name: case; Type: TABLE DATA; Schema: public; Owner: morgan
--

COPY public."case" (case_id, patient_id) FROM stdin;
1	1
\.


--
-- Data for Name: mutation; Type: TABLE DATA; Schema: public; Owner: morgan
--

COPY public.mutation (mutation_id) FROM stdin;
1
\.


--
-- Data for Name: mutations_result; Type: TABLE DATA; Schema: public; Owner: morgan
--

COPY public.mutations_result (id_mutation_result, zygosity, mutation_id, analysis_analysis_name) FROM stdin;
1	test	1	1
\.


--
-- Data for Name: patient; Type: TABLE DATA; Schema: public; Owner: morgan
--

COPY public.patient (patient_id, patient_name) FROM stdin;
1	TEST
\.


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: morgan
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

