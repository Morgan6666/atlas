

-- 1) Вариант по данным из задания
-- select count(mr.mutationId) from analysis a
-- inner join "MutationResult" mr on a.analysisName=mr.Analysis_analysisName
-- inner join "Barcode" b on a.barcodeName=b.barcodeName
-- inner join "Case" c on b.patientId=c.patientId
-- where a.analysisRole='Major'


-- 2) Вариант про локальной таблице
-- select count(mr.mutation_id) from analysis a
-- inner join "mutations_result" mr on a.analysis_name=mr.analysis_analysis_name
-- inner join "barcode" b on a.barcode_name=b.barcode_name
-- inner join "case" c on b.patient_id=c.patient_id
-- where a.analysis_role='Major'

-- 3) Дамп для воспроизведенеия
--mutations_localhost-2024_06_25_14_59_47-dump.sql