import pandas as pd

from task2.constant import INPUT_FILE, OUTPUT_FILE

################################## Задание №3 ####################################################
class VariantDetect:
    def __init__(self, data_file: str, output_file: str):
        self.data_file = data_file
        self.data = pd.read_csv(data_file, sep='\t')
        self.output_file  = output_file

    def calculate_detected_variants(self):
        detected_variants = []
        for index, row in self.data.iterrows():
            if row['count'] <= 10 and row['isPathogenic'] != '':
                variant_id = row['position']
                sample_ids = [col for col in self.data.columns[3:] if row[col] != '0.00' and row[col] != 'unwanted consequence']
                detected_variant_str = ' '.join([f'{col} - {variant_id}' for col in sample_ids])
                if detected_variant_str:
                    detected_variants.append(detected_variant_str)

        with open(self.output_file, 'w') as f:
            f.write('\n'.join(detected_variants))


if __name__ == '__main__':
    variant_calculator = VariantDetect(INPUT_FILE, OUTPUT_FILE)
    detected_variants = variant_calculator.calculate_detected_variants()
    
    
    
################################### Задание №4  ###########################################################
# Ответ: S9 возможно контаминирован в  S1 для chr17:41245471C>T
# Просмотрел варианты образцы,для S1 0.53 а для S9 0.54, +- равны для двух образцов